#include <stdio.h>
#include <stdlib.h>

typedef struct arvore {
   char info;
   struct arvore *esq;
   struct arvore *dir;
} Arvore;


Arvore*  cria_arv_vazia (void);
Arvore*  arv_constroi (char c, Arvore* e, Arvore* d);
int      verifica_arv_vazia (Arvore* a);
Arvore*  arv_libera (Arvore* a);
int      arv_pertence (Arvore* a, char c);
void     arv_imprime (Arvore* a);

Arvore* cria_arv_vazia (void) {
   return NULL;
}

Arvore* arv_constroi (char c, Arvore* e, Arvore* d) {
  Arvore* a = (Arvore*)malloc(sizeof(Arvore));
  a->info = c;
  a->esq = e;
  a->dir = d;
  return a;
}

int verifica_arv_vazia (Arvore* a) {
  return (a == NULL);
}

Arvore* arv_libera (Arvore* a) {
  if (!verifica_arv_vazia(a)) {
    arv_libera (a->esq);
    arv_libera (a->dir);
    free(a);
  }
  return NULL;
}

Arvore* cria_espelho(Arvore * arv_a){
  Arvore* espelho;
  espelho = arv_a;
	
  if (espelho == cria_arv_vazia()) {
    return;
  }

  if (espelho->esq != cria_arv_vazia()) {
    cria_espelho(espelho->esq);
  }

  if (espelho->dir != cria_arv_vazia()) {
    cria_espelho(espelho->dir);
  }

  Arvore* temp = espelho->esq;
  espelho->esq = espelho->dir;
  espelho->dir = temp;
  
  return espelho;
  
}

int eh_espelho(Arvore * arv_a, Arvore * arv_b){
	if(arv_a == cria_arv_vazia() && arv_b == cria_arv_vazia())
		return 1;
	else if(arv_a == cria_arv_vazia() || arv_b == cria_arv_vazia())
		return 0;
	else {
		return arv_a->info == arv_b->info && eh_espelho(arv_a->esq, arv_b->dir) && eh_espelho(arv_a->dir, arv_b->esq);	
	}
}



int main (int argc, char *argv[]) {
  Arvore *a, *a1, *a2, *a3, *a4, *a5, *espelho;
  a1 = arv_constroi('d',cria_arv_vazia(),cria_arv_vazia());
  a2 = arv_constroi('b',cria_arv_vazia(),a1);
  a3 = arv_constroi('e',cria_arv_vazia(),cria_arv_vazia());
  a4 = arv_constroi('f',cria_arv_vazia(),cria_arv_vazia());
  a5 = arv_constroi('c',a3,a4);
  a  = arv_constroi('a',a2,a5);
  
  espelho = cria_espelho(a);
  eh_espelho(a,espelho);
  
  return 0;
}
